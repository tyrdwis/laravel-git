<?php
function xo($str) {
    $tampungX = 0;
    $tampungO = 0;

    for($i = strlen($str); $i >= 0; $i--){
        $start = substr($str, $i, $i-1);
        // echo $start;
        if ($start == "x"){
            $tampungX +=1;
        }else if($start == "o"){
            $tampungO += 1;
        }
        // echo $tampungO;
        // echo $tampungX;
    }


    if ($tampungX == $tampungO){
        echo "Benar";
    }else{
        echo "Salah";
    }
    echo "<br>";


}

// Test Cases
echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxo'); // "Salah"
echo xo('xxooox'); // "Benar"
echo xo('xoxooxxo'); // "Benar"
?>