<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="layout.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <header>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="booking.php">Booking</a>
            </li>
        </ul>
    </header>
    <main class="home">
        <div class="mt-3">
            <ul class="nav justify-content-center">
                <p class="nav-item" style="color: white; padding-top: 3px;">Reserve your venue now!</p>
            </ul>
        </div>
        <div class="card mt-3 mb-4">
            <div class="row">
                <div class="col m-5">
                    <img src="GarudaHall.jpg" class="img-thumbnail mt-5" alt="">
                </div>
                <div class="col">
                    <form action="myBooking.php" method="POST">
                        <div class="m-3">
                            <label for="name" class="form-label">Name</label>
                            <input class="form-control" type="text" id="name" name="name" placeholder="Tiara_1202194097" aria-label="Disabled input example" disabled>
                        </div>
                        <div class="m-3">
                            <label for="eventDate" class="form-label">Event Date</label>
                            <input class="form-control" type="date" id="eventDate" name="eventDate">
                        </div>
                        <div class="m-3">
                            <label for="startTime" class="form-label">Start Time</label>
                            <input class="form-control" type="time" id="startTime" name="startTime">
                        </div>
                        <div class="m-3">
                            <label for="duration" class="form-label">Duration (Hours)</label>
                            <input class="form-control" type="number" id="duration" name="duration" min="1" placeholder="1">
                        </div>
                        <div class="m-3">
                            <label for="buildingType">Building Type</label>
                            <select class="form-select" id="buildingType" name="buildingType" aria-label="Default select example">
                                <option selected>Choose...</option>
                                <option value="Nusantara Hall">Nusantara Hall</option>
                                <option value="Gadura Hall">Gadura Hall</option>
                                <option value="Gedung Serba Guna">Gedung Serba Guna</option> -->
                            </select>
                        </div>
                        <div class="m-3">
                            <label for="phone" class="form-label">Phone Number</label>
                            <input class="form-control" type="number" id="phone" name="phone">
                        </div>
                        <div class="m-3">
                            <label for="addService">Add Service(s)</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="choice[]" value="1" id="catering">
                                <label class="form-check-label" for="catering" name="service">
                                    Catering / $700
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="choice[]" value="2" id="decoration">
                                <label class="form-check-label" for="decoration" name="service">
                                    Decoration / $450
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="choice[]" value="3" id="soundSystem">
                                <label class="form-check-label" for="soundSystem" name="service">
                                    Sound System / $250
                                </label>
                            </div>
                        </div>
                        <div class="m-3">
                            <input type="submit" class="btn btn-primary w-100" name="Book"></input>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer bg-light text-center text-lg-start">
        <p style="text-align: center; padding-top: 10px;">Created by: Tiara_1202194097</p>
    </footer>
</body>
</html>