<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Layout</title>
    <link rel="stylesheet" href="layout.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <header>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="booking.php">Booking</a>
            </li>
        </ul>
    </header>
    <main style="height:467px; margin-top:25px;">
        <div style="text-align:center; padding-top:10px;">
            <h5>Thank you Tiara_1202194097 for Reserving</h5>
            <p>Please double check your reservation</p>
        </div>
        <div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Booking Number</th>
                    <th scope="col">Name</th>
                    <th scope="col">Check-in</th>
                    <th scope="col">Check-out</th>
                    <th scope="col">Building Type</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Service</th>
                    <th scope="col">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">
                        <?php 
                            echo (rand());
                        ?>
                    </th>
                    <td>Tiara_1202194097</td>
                    <td>
                        <?php 
                        $eventDate = $_POST["eventDate"];
                        $startTime = $_POST["startTime"];
                        echo $eventDate. " ". $startTime;
                        ?>
                    </td>
                    <td>
                        <?php 
                        $eventDate = $_POST["eventDate"];
                        $duration = $_POST["duration"];
                        $startTime = $_POST["startTime"];

                        $checkIn= $eventDate. " ". $startTime;
                        $durasi = "+ ".$duration." hour";
                        $checkOut = date('Y-m-d G:i',strtotime($durasi, strtotime($checkIn)));
                        echo $checkOut;

                        ?>
                    </td>
                    <td><?= $_POST["buildingType"]?>
                    </td>
                    <td> 
                        <?= $_POST["phone"]?>
                    </td>
                    <td>
                        <?php
                            if(isset($_POST["choice"])){
                                $service=$_POST["choice"];
                                $c = count($service);
                                $choosen=array();
                                $price=0;
                                for($i=0; $i<$c; $i++) {
                                    if($service[$i]==1){
                                        $price=$price+700;
                                        array_push($choosen,"Catering");
                                    }
                                    else if($service[$i]==2){
                                        $price=$price+450;
                                        array_push($choosen,"Decoration");
                                    }
                                    else if($service[$i]==3){
                                        $price=$price+250;
                                        array_push($choosen,"Sound System");
                                    }
                                }
                                foreach($choosen as $value){
                                    echo "<li> $value </li>";
                                }
                            }
                            else{
                                echo "no service";
                            }
                        ?>
                    </td>
                    <td>
                        <?php 
                        $building = $_POST["buildingType"];
                        $hour = (int)$_POST["duration"];
                        if ($building == "Nusantara Hall"){
                            $total = $hour * 2000;
                        } else if ($building == "Gadura Hall"){
                            $total = $hour * 1000;
                        } else if ($building == "Gedung Serba Guna"){
                            $total = $hour * 500;
                        } else {
                            echo "choose at least one building";
                        }
                        $total2 = $total+$price;
                        echo $total2;
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </main>
    <footer class="footer bg-light text-center text-lg-start">
        <p style="text-align: center; padding-top: 10px;">Created by: Tiara_1202194097</p>
    </footer>
</body>
</html>