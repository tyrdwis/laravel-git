<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="layout.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <header>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="booking.php">Booking</a>
            </li>
        </ul>
    </header>
    <main class="home">
        <h5 style="text-align: center; padding: 10px;">WELCOME TO ESD VENUE RESERVATION</h5>
        <div>
            <ul class="nav justify-content-center">
                <p class="nav-item" style="color: white; padding-top: 3px;">Find your best deal for your event, here!</p>
            </ul>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm card m-3 p-0" style="width: 18rem;">
                    <img src="NusantaraHall.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title fw-bold">Nusantara Hall</h5>
                        <p class="card-text m-0">$2000 / Hour</p>
                        <p class="card-text">5000 Capacity</p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item text-success fw-bold">Free Parking</li>
                        <li class="list-group-item text-success fw-bold">Full AC</li>
                        <li class="list-group-item text-success fw-bold">Cleaning Service</li>
                        <li class="list-group-item text-success fw-bold">Covid-19 Health Protocol</li>
                    </ul>
                    <div class="card-body bg-light text-center">
                        <form action="booking.php" method="POST">
                            <button class="btn btn-primary" name="bookNow" value="nusantara">Book now</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm card m-3 p-0" style="width: 18rem;">
                    <img src="GarudaHall.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title fw-bold">Garuda Hall</h5>
                        <p class="card-text m-0">$1000 / Hour</p>
                        <p class="card-text">2000 Capacity</p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item text-success fw-bold">Free Parking</li>
                        <li class="list-group-item text-success fw-bold">Full AC</li>
                        <li class="list-group-item text-danger fw-bold">No Cleaning Service</li>
                        <li class="list-group-item text-success fw-bold">Covid-19 Health Protocol</li>
                    </ul>
                    <div class="card-body bg-light text-center">
                        <form action="booking.php" method="POST">
                            <button class="btn btn-primary" name="bookNow" value="garuda">Book now</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm card m-3 p-0" style="width: 18rem;">
                    <img src="GedungSerbaguna.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title fw-bold">Gedung Serba Guna</h5>
                        <p class="card-text m-0">$500 / Hour</p>
                        <p class="card-text">500 Capacity</p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item text-danger fw-bold">No Free Parking</li>
                        <li class="list-group-item text-danger fw-bold">No Full AC</li>
                        <li class="list-group-item text-danger fw-bold">No Cleaning Service</li>
                        <li class="list-group-item text-success fw-bold">Covid-19 Health Protocol</li>
                    </ul>
                    <div class="card-body bg-light text-center">
                        <form action="booking.php" method="POST">
                            <button class="btn btn-primary" name="bookNow" value="serbaguna">Book now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer bg-light text-center text-lg-start">
        <p style="text-align: center; padding-top: 10px;">Created by: Tiara_1202194097</p>
    </footer>
</body>
</html>